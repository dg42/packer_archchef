# packer_archchef

[![pipeline status](https://gitlab.com/dg42/packer_archchef/badges/master/pipeline.svg)](https://gitlab.com/dg42/packer_archchef/commits/master)

An Arch Linux AMI with Chef Infra Client pre-installed, built with Packer.  The only purpose this project serves is to build an Arch Linux AMI that can be used AWS for testing some of my cookbook projects.

The general concept of building the AMI is adapted from Conrad Hoffmann's (@bitfehler) [`archlinux-ec2`](https://gitlab.com/bitfehler/archlinux-ec2) project, with a few modifications:
- The AMI is built using the GitLab CI/CD pipeline rather than using make
- Amazon Linux is used in the Packer build rather than Debian
- Updated to work with T3 instance family
