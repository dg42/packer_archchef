#!/bin/bash

set -x
set -e

# Partition the EBS volume
sudo sgdisk -Zg -n 1:0:4095 -t 1:EF02 -c 1:GRUB -n 2:0:0 -t 2:8300 -c 2:ROOT /dev/xvdf
sync

# Create file system on main partition
sudo mkfs.ext4 /dev/nvme1n1p2

# Extract base Arch Linux system to temp directory
mkdir arch
cd arch && sudo tar xpzf /tmp/archbase.tar.gz
cd -

# Mount the file systems
sudo mount --bind arch arch
sudo mount --bind /proc arch/proc
sudo mount --bind /sys arch/sys
sudo mount --bind /dev arch/dev
sudo mount --bind /etc/resolv.conf arch/etc/resolv.conf
sudo mount /dev/nvme1n1p2 arch/mnt

# Install essential packages and optional tools
sudo chroot arch pacstrap -c /mnt base grub lsb-release python2 sudo openssh net-tools binutils linux inetutils cloud-init

# Generate fstab file
sudo chroot arch genfstab -U /mnt | sudo tee arch/mnt/etc/fstab

# Localization
echo "en_US.UTF-8 UTF-8" | sudo tee -a arch/mnt/etc/locale.gen
echo "LANG=en_US.UTF-8"  | sudo tee -a arch/mnt/etc/locale.conf
sudo chroot arch arch-chroot /mnt locale-gen

# Set the hostname
echo "archec2" | sudo tee -a arch/mnt/etc/hostname

# Install the AUR packages
sudo cp arch/*.pkg.tar.xz arch/mnt/
for pkg in $PACKAGES; do
  sudo chroot arch arch-chroot /mnt pacman -U --noconfirm --noprogressbar /$pkg.pkg.tar.xz
done
sudo rm arch/mnt/*.pkg.tar.xz

# Create link for Chef Infra Client binaries
binaries="chef-apply chef-client chef-shell chef-solo inspec knife ohai"
for binary in $binaries; do
  sudo chroot arch arch-chroot /mnt ln -s /opt/chef/bin/$binary /usr/bin/
done

# Enable cloud-init services
sudo chroot arch arch-chroot /mnt systemctl enable cloud-init
sudo chroot arch arch-chroot /mnt systemctl enable cloud-final

# Install GRUB
sudo chroot arch arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/xvdf
sudo chroot arch arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

# Enable systemd-networkd
sudo chroot arch arch-chroot /mnt systemctl enable systemd-networkd.service
sudo chroot arch arch-chroot /mnt systemctl enable systemd-resolved.service
sudo chroot arch ln -sf /run/systemd/resolve/stub-resolv.conf /mnt/etc/resolv.conf

# Cleanup and exit
sudo sync
sleep 3
sudo umount arch/etc/resolv.conf
sudo umount arch/mnt
sudo umount arch/proc
sudo umount arch/sys
sudo umount arch/dev
sudo umount arch
